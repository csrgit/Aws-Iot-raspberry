# Install requirements on your raspberry and run test on Aws-Iot Console

Use the AWS IOT SDK:

Install cmake:

```
sudo apt-get install cmake
```


Install OpenSSL 1.0.2 developer version:

Add the following text at the end of /etc/apt/sources.list.d/raspi.list:

deb http://ftp.debian.org/debian jessie-backports main


Update the package list:

```
sudo apt-get update
```


Install Open SSL:

```
sudo apt-get -t jessie-backports install libssl-dev
```


Install AWS IOT SDK:

```
sudo pip install AWSIoTPythonSDK
```


Create certificates

https://docs.aws.amazon.com/iot/latest/developerguide/create-device-certificate.html


Attach a Certificate to a Thing

https://docs.aws.amazon.com/iot/latest/developerguide/attach-cert-thing.html


Move the certificates into a certs folder



# Config and run test on Aws Iot Console 


Subscribe a new Topic for a test , use the name "myTopic" for this example 

![picture](/img/aws2.png)


Run on console this command 


```
python client.py
```


Watch the activity in console 

![picture](/img/aws3.png)  


